import React from "react";
import tw, { css, theme } from "twin.macro";

export default () => {
  const friday = new Date().getDay() == 5;

  if (friday === true) {
    return <p>It's Friday, you better be prepared</p>;
  }

  return <p>It's Friday, you better be prepared</p>;
};
