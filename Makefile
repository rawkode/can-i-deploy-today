.PHONY: clean deps dev build deploy

clean:
	@yarn cache clean

deps:
	@yarn install

dev: deps
	@yarn dev

build: deps
	@yarn build
	@yarn export

deploy: deps
	@firebase deploy --only hosting
