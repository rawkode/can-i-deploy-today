import React from "react";
import Head from "next/head";
import tw, { css, theme } from "twin.macro";
import "tailwindcss/dist/base.min.css";

import Day from "../components/day";

const linearGradientStyle = css`
  background: linear-gradient(
    ${theme`colors.ribbon`},
    ${theme`colors.electric`}
  );
`;

const App = () => {
  let today = new Date();

  let friday = today.getDay() == 5;

  return (
    <div>
      <Head>
        <script
          async
          defer
          data-domain="canideploy.today"
          src="https://plausible.io/js/plausible.js"
        ></script>
        <meta name="twitter:card" content="summary_large_image" key="twcard" />
        <meta name="twitter:creator" content="@rawkode" key="twhandle" />
        <meta
          name="twitter:image"
          content="https://canideploy.today/image.png"
          key="twitterimage"
        />

        {/* Open Graph */}
        <meta
          property="og:url"
          content="https://canideploy.today"
          key="ogurl"
        />
        <meta
          property="og:site_name"
          content="Can I Deploy Today?"
          key="ogsitename"
        />
        <meta property="og:title" content="Can I Deploy Today?" key="ogtitle" />
        <meta property="og:description" content="Yes" key="ogdesc" />
        <meta property="og:image" content="/image.png" key="ogimage" />
      </Head>
      <div
        css={[
          tw`flex flex-col items-center justify-center h-screen`,
          linearGradientStyle,
        ]}
      >
        <div tw="flex flex-col justify-center h-full space-y-5 items-center">
          <h1 tw="font-semibold text-2xl text-white">Can I Deploy, Today?</h1>
          <h2 tw="font-bold text-6xl text-green-500">Yes</h2>
        </div>
      </div>
    </div>
  );
};

export default App;
